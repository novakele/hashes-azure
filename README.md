# hashes-Azure

## What it does
With `managing.sh` you can either **deploy** a template with resources or **unploy** to removes the resources.  
## Recommended setup
I use an alias in my `.zshrc` to use `managing.sh` script.  
This could be something like this:
```
alias hashcat="managing_hashcat_azure"

function managing_hashcat_azure() {
    $(which zsh) ~/repositories/hashes-azure/managing.sh $1
}
```
I have a host entry in my `~/.ssh/config` so I can use an alias to connect to the machine.  
This could be something like this:
```
Host [alias]
    User [username]
    Hostname [ip]
    Port [port]
    IdentityFile [private key]
```
At the end of a successful deployment, `managing.sh` will try to update the ip of the azure host entry.
This way you can use `ssh [alias]` without having to copy paste the ip or change your file manually.

## Credits
All credits go to [Carl Mönnig](https://github.com/carlmon) for the setup and idea. [hashcat-Azure](https://github.com/carlmon/Hashcat-Azure)
  
I merely adapted it to fit my needs and be on Gitlab
