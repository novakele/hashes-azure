#!/usr/local/bin/zsh

deploy_name=hashcat
location=southcentralus
res_group=$deploy_name
template_file=~/repositories/hashes-azure/azuredeploy.json
azure_admin=$(whoami)
azure_pub_key=$(cat ~/.ssh/azure.pub)
public_ip_obj=HashcatPublicIP
purge_template=~/repositories/hashes-azure/purge-resources.json
CR="\e[100D" # brings the cursor to the beginning of the line
KL="\e[K" # clears the rest of the line
ssh_hostname="Host azure"
ssh_config=~/.ssh/config


if [[ $# != 1 ]]; then
    echo -e "Usage:\t./managing.sh action\navailable actions:\n\tdeploy - Deploy a template and create the resources associated with it\n\tunploy - Deploy an empty template to clear the resources"
    return -1
fi

if [[ $1 == "deploy" ]]; then
    # check if resource group exists
    echo -ne "[-] Checking if resource group '$res_group' exists $CR"
    az group list | cut -d":" -f2 | grep -o hashcat &> /dev/null

    # creating the resource group if not found
    if [[ $? != 0 ]]; then
	    echo -ne "[-] Creating resource group '$res_group' $KL $CR"
	    az group create --name $name --location $location &> /dev/null
	    if [[ $? == 0 ]]; then
            echo -e "[+] Resource group '$res_group' has been created $KL"
	    else
            echo -e "[!] Resource group '$res_group' could not be created $KL"
            return -1
	    fi
    else
	    echo -e "[+] Resource group '$res_group' exists $KL"
    fi

    # deploy from template
    echo -ne "[-] Deploying '$deploy_name' from template $template_file $CR"

    az deployment group create --name $deploy_name --resource-group $res_group --template-file $template_file --parameters adminUsername=$azure_admin adminPublicKey=$azure_pub_key &>/dev/null

    if [[ $? == 0 ]]; then
	    echo -e "[+] Deployment was successful $KL"
    else
	    echo -e "[!] Deployment was unsuccessfuli $KL"
	    return -1
    fi

    # Get public IP of hashcat vm
    ip=$(az network public-ip show -g $res_group --name $public_ip_obj --query "{ipAddress : ipAddress}" | grep -Eo "([0-9]{1,3}[\.]){3}[0-9]{1,3}")
    if [[ $? == 0 ]]; then
        echo -e "[+] $ip"

        # Updating the IP for the host in ~/.ssh/config
        # Getting the line number for the azure entry
        i=$(grep -n $ssh_hostname $ssh_config | cut -d":" -f1)
        (( i = $i + 2 ))
        echo -ne "[-] Updating Azure's entry in $ssh_config $CR"

        sed -i.bck $i"s/Hostname.*/Hostname $ip/" $ssh_config

        if [[ $? == 0 ]]; then
            echo -e "[+] Azure's entry is up to date! $KL"
        else
            echo -e "[!] Unable to update Azure's entry in $ssh_config $KL"
        fi
    else
        echo -e "[!] Unable to get the IP for the deployed resource group"
    fi
fi

# delete the content of a resource group by feeding it an empty template of resources
if [[ $1 == "unploy" ]];then
    echo -ne "[-] Erasing the resources in '$res_group' $CR"
    az deployment group create --name $deploy_name -g $res_group -f $purge_template --mode Complete &> /dev/null
    if [[ $? == 0 ]]; then
        echo -e "[+] Erasing the resources in '$res_group' is done! $KL"
    else
        echo -e "[!] Erasing the resources in '$res_group' as returned an error. $KL"
        return -1
    fi
fi

